# python-sendemail

Send one email msg with optional file attachment.

...

    usage: sendemail [-h] [-V] [-a ATTACHMENT] [-m MESSAGE_TEXT] [-s SUBJECT]
                     [-t TO_ADDRESS]

    optional arguments:
      -h, --help            show this help and quit
      -V, --Version         Show version and quit
      -a ATTACHMENT, --attachment ATTACHMENT
                            File attachment pathname
      -m MESSAGE_TEXT, --message_text MESSAGE_TEXT
                            Message text file pathname (standard input if -m not
                            used)
      -s SUBJECT, --subject SUBJECT
                            Message subject, default mail from Mr. Python
      -t TO_ADDRESS, --to_address TO_ADDRESS
                            Debug level, default test-target@soper.us

...
