#!/usr/bin/python
#
# Read standard input or a specified file (-m) and send it as an email
# message with an optional attachment.
#
# Copyright (C) Peter Soper (pete@soper.us) 2016 MIT License
# Original June, 2015, updated September, 2016, April 2020 (and remembered to 
#   push it into Bitbucket!)
#
#usage: sendemail [-h] [-V] [-a ATTACHMENT] [-m MESSAGE_TEXT] [-s SUBJECT]
#                 [-t TO_ADDRESS]
#
#Send an email message
#
#optional arguments:
#  -h, --help            show this help message and quit
#  -V, --Version         Show version and quit
#  -a ATTACHMENT, --attachment ATTACHMENT
#                        File attachment pathname
#  -m MESSAGE_TEXT, --message_text MESSAGE_TEXT
#                        Message text file pathname (standard input if -m not
#                        used)
#  -s SUBJECT, --subject SUBJECT
#                        Message subject, default mail from Mr. Python
#  -t TO_ADDRESS, --to_address TO_ADDRESS
#                        Debug level, default test-target@soper.us
#
import argparse, smtplib, sys, os

from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email import Encoders

VERSION = '0.995'

# ==== BEGIN USER CONFIGURABLE SETTINGS ====
# These parameters specify the outbound mail SMT server to use to deliver
# the mail.

SMTP_SERVER = 'smtp.someserver.com' # varies a lot
SMTP_USER = 'somebody@someserver.com' # not necessarily same domain as above
SMTP_PASSWORD = 'nowisthetimeforallgoodpeopletocometotheaidoftheircountry'

# Defaults for a lazy tester

DEFAULT_SUBJECT = 'ROBOT TALKING: Bark Bark'
DEFAULT_TO_ADDRESS = 'Joe Blow <joesephbloweth@gmail.com>'
DEFAULT_REPLY_TO = 'yetanotheraddress@someplace.com'
# ==== END USER CONFIGURABLE SETTINGS ====

def send_email(to, subject, text, attach=None):
    "Deliver one email message via SMTP"
    msg = MIMEMultipart()
    msg['From'] = SMTP_USER
    msg['To'] = to
    msg['Reply-to'] = reply_to
    msg['Subject'] = subject
    msg.attach(MIMEText(text))
    if attach:
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(open(attach, 'rb').read())
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 
            'attachment; filename="%s"' % os.path.basename(attach))
        msg.attach(part)
    mailServer = smtplib.SMTP(SMTP_SERVER, 587)
    mailServer.ehlo()
    mailServer.starttls()
    mailServer.ehlo()
    mailServer.login(SMTP_USER, SMTP_PASSWORD)
    mailServer.sendmail(SMTP_USER, to, msg.as_string())
    mailServer.close()

if __name__ == '__main__':
    "Handle command line arguments(options) and send message"

    parser = argparse.ArgumentParser( description='Send an email message')

    parser.add_argument('-V','--Version', help='Show version and quit',
			action = 'store_true')

    parser.add_argument('-a', '--attachment', default=None, 
			help='File attachment pathname')

    parser.add_argument('-m', '--message_text', default=None, 
	help='Message text file pathname (standard input if -m not used)')

    parser.add_argument('-s', '--subject', default=DEFAULT_SUBJECT,
			help='Message subject, default ' + DEFAULT_SUBJECT)

    parser.add_argument('-t', '--to_address', default=DEFAULT_TO_ADDRESS, 
			help='Debug level, default ' + DEFAULT_TO_ADDRESS)

    args = parser.parse_args()

    if args.Version:
	print('sendemail version ' + VERSION)
	quit()

    if args.message_text:
	text_file = open(args.message_text,'r')
    else:
	print 'Reading standard input until EOF (two control-D w Python on Linux)'
	text_file = sys.stdin

    text = ''
    for line in text_file:
        text += line

    print 'Sending message'
    sys.stdout.flush()

    send_email(args.to_address, args.subject, text, attach=args.attachment)
